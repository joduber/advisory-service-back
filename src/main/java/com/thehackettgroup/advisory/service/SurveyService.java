package com.thehackettgroup.advisory.service;

import com.thehackettgroup.advisory.dao.SurveyDao;
import com.thehackettgroup.advisory.dto.Survey;
import com.thehackettgroup.advisory.exception.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SurveyService {

    @Autowired
    private SurveyDao surveyDao;

    public Survey getSurveyById(String surveyId) {
        Optional<Survey> survey = surveyDao.getSurveyById(surveyId);
        if (survey.isPresent()) {
            return survey.get();
        } else {
            String errorMessage = String.format("Survey with ID: %s was not found.", surveyId);
            throw new RecordNotFoundException(errorMessage);
        }
    }
}
