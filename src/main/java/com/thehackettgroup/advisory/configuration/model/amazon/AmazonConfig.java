package com.thehackettgroup.advisory.configuration.model.amazon;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("amazon")
public class AmazonConfig {

    private String region;
    private List<String> profiles;
    private Database database;
    private Vendors vendors;

    public Vendors getVendors() {
        return vendors;
    }

    public void setVendors(Vendors vendors) {
        this.vendors = vendors;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    @Override
    public String toString() {
        return String.format("[AmazonConfig] = Region: %s - Supported Profiles: %s - SecretStore: %s - Vendors: %s",
                region, profiles.toString(), database.toString(), vendors.toString());
    }

}
