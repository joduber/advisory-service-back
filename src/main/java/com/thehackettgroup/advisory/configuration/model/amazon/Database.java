package com.thehackettgroup.advisory.configuration.model.amazon;

public class Database {

    private String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return String.format("[AmazonConfig.Database] = Secret: %s", secret);
    }
}
