package com.thehackettgroup.advisory.configuration.model.amazon;

public class Vendors {

    private String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return String.format("[AmazonConfig.Vendors] = Secret: %s", secret);
    }
}
