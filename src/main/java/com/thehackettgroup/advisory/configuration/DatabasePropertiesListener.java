package com.thehackettgroup.advisory.configuration;

import com.thehackettgroup.advisory.exception.AdvisoryApplicationException;
import com.thehackettgroup.advisory.util.SecretManagerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.*;

public class DatabasePropertiesListener implements ApplicationListener<ApplicationReadyEvent> {

    public static final Logger logger = LoggerFactory.getLogger(DatabasePropertiesListener.class);

    private final static String SPRING_DATASOURCE_USERNAME = "spring.datasource.username";
    private final static String SPRING_DATASOURCE_PASSWORD = "spring.datasource.password";
    private static final String AMAZON_DB_SECRET = "amazon.database.secret";
    private static final String AMAZON_VENDORS_SECRET = "amazon.vendors.secret";
    private static final String AMAZON_REGION = "amazon.region";
    private static final String DB_USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String FRESHDESK_API_KEY = "freshdesk.apiKey";
    private static final String FRESHDESK_SSO_SHARED_SECRET = "freshdesk.ssoSharedSecret";
    private static final String LUCIDCHART_API_KEY = "lucidchart.apiKey";
    private static final String LUCIDCHART_API_SECRET = "lucidchart.apiSecret";
    private static final String AMAZON_S_3_ACCESS_KEY = "amazon.s3AccessKey";
    private static final String AMAZON_S_3_SECRET_KEY = "amazon.s3SecretKey";
    private static final String QUALTRICS_APITOKEN = "qualtrics.apitoken";
    private static final String CLICKMETER_API_KEY = "clickmeter.apiKey";

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        // Get username and password from AWS Secret Manager using secret name
        ConfigurableEnvironment configurableEnvironment = event.getApplicationContext().getEnvironment();
        String[] activeProfiles = configurableEnvironment.getActiveProfiles();
        List<String> acceptedProfiles = Arrays.asList(Objects.requireNonNull(configurableEnvironment.getProperty("amazon.profiles")).split("\\s*,\\s*"));
        if (acceptedProfiles.stream().anyMatch(element -> Arrays.asList(activeProfiles).contains(element))) {
            String activeProfile = Arrays.toString(activeProfiles);
            logger.info("Application startup intercepted for: {} environment profile, will proceed to retrieve AWS secrets.", activeProfile);
            HashMap<String, String> dbSecrets = SecretManagerUtil.obtainSecrets(configurableEnvironment.getProperty(AMAZON_DB_SECRET),
                    configurableEnvironment.getProperty(AMAZON_REGION));

            HashMap<String, String> vendorSecrets = SecretManagerUtil.obtainSecrets(configurableEnvironment.getProperty(AMAZON_VENDORS_SECRET),
                    configurableEnvironment.getProperty(AMAZON_REGION));
            Properties awsProperties = getAwsProperties(dbSecrets, vendorSecrets);

            configurableEnvironment.getPropertySources().addFirst(new PropertiesPropertySource("aws.secret.manager", awsProperties));
        }
    }

    private Properties getAwsProperties(HashMap<String, String> dbSecrets, HashMap<String, String> vendorSecrets) {
        if (dbSecrets == null || vendorSecrets == null) {
            throw new AdvisoryApplicationException("AWS secrets are empty or null, unable to start up application without database access.");
        }
        Properties props = new Properties();
        appendDatabaseSecrets(dbSecrets, props);
        appendVendorSecrets(vendorSecrets, props);
        return props;
    }

    private void appendDatabaseSecrets(HashMap<String, String> dbSecrets, Properties props) {
        props.put(SPRING_DATASOURCE_USERNAME, dbSecrets.get(DB_USERNAME));
        props.put(SPRING_DATASOURCE_PASSWORD, dbSecrets.get(PASSWORD));
    }

    private void appendVendorSecrets(HashMap<String, String> vendorSecrets, Properties props) {
        props.put(FRESHDESK_API_KEY, vendorSecrets.get(FRESHDESK_API_KEY));
        props.put(FRESHDESK_SSO_SHARED_SECRET, vendorSecrets.get(FRESHDESK_SSO_SHARED_SECRET));
        props.put(LUCIDCHART_API_KEY, vendorSecrets.get(LUCIDCHART_API_KEY));
        props.put(LUCIDCHART_API_SECRET, vendorSecrets.get(LUCIDCHART_API_SECRET));
        props.put(AMAZON_S_3_ACCESS_KEY, vendorSecrets.get(AMAZON_S_3_ACCESS_KEY));
        props.put(AMAZON_S_3_SECRET_KEY, vendorSecrets.get(AMAZON_S_3_SECRET_KEY));
        props.put(QUALTRICS_APITOKEN, vendorSecrets.get(QUALTRICS_APITOKEN));
        props.put(CLICKMETER_API_KEY, vendorSecrets.get(CLICKMETER_API_KEY));
    }
}
