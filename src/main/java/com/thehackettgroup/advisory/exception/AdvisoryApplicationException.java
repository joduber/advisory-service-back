package com.thehackettgroup.advisory.exception;

public class AdvisoryApplicationException extends RuntimeException {

    public AdvisoryApplicationException() {
        super();
    }

    public AdvisoryApplicationException(String msg) {
        super(msg);
    }
}
