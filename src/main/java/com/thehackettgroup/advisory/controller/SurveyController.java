package com.thehackettgroup.advisory.controller;

import com.thehackettgroup.advisory.dto.Survey;
import com.thehackettgroup.advisory.service.SurveyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/")
public class SurveyController {

    @Autowired
    private SurveyService surveyService;

    public static final Logger logger = LoggerFactory.getLogger(SurveyController.class);

    @GetMapping("/survey/{surveyId}")
    public Survey getSurvey(@PathVariable(value = "surveyId") String surveyId) {
        logger.info("Request to retrieve survey with id: {} will be dispatched", surveyId);
        return surveyService.getSurveyById(surveyId);
    }

}
