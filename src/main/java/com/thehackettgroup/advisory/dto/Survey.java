package com.thehackettgroup.advisory.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Survey {
    private UUID surveyId;
    private String name;
    private String description;
    private String surveyType;
    private String qualtricSurveyId;
    private String link;
    private UUID masterClientId;

    public UUID getMasterClientId() {
        return masterClientId;
    }

    public void setMasterClientId(UUID masterClientId) {
        this.masterClientId = masterClientId;
    }

    public UUID getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(UUID surveyId) {
        this.surveyId = surveyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getQualtricSurveyId() {
        return qualtricSurveyId;
    }

    public void setQualtricSurveyId(String qualtricSurveyId) {
        this.qualtricSurveyId = qualtricSurveyId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
