package com.thehackettgroup.advisory.rowmapper;

import com.thehackettgroup.advisory.builder.GenericBuilder;
import com.thehackettgroup.advisory.dto.Survey;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;


public class SurveyRowMapper implements RowMapper<Optional<Survey>> {

    @Override
    public Optional<Survey> mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Optional.of(GenericBuilder.of(Survey::new)
                .with(Survey::setName, rs.getString("name"))
                .with(Survey::setSurveyId, UUID.fromString(rs.getString("survey_id")))
                .with(Survey::setSurveyType, rs.getString("survey_type_id"))
                .with(Survey::setQualtricSurveyId, rs.getString("qualtrics_survey_id"))
                .with(Survey::setMasterClientId, UUID.fromString(rs.getString("master_client_id")))
                .with(Survey::setDescription, rs.getString("description"))
                .build());
    }
}
