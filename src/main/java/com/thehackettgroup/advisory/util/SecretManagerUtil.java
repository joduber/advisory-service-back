package com.thehackettgroup.advisory.util;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;

import static org.slf4j.LoggerFactory.getLogger;

public class SecretManagerUtil {

    private static final Logger logger = getLogger(SecretManagerUtil.class);

    public static HashMap<String, String> obtainSecrets(String secretName, String region) {
        logger.info("Fetching AWS secret: '{}' from Secrets store", secretName);
        String secretValue;
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .build();

        String secret, decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException | InternalServiceErrorException | InvalidParameterException
                | InvalidRequestException | ResourceNotFoundException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key. DecryptionFailureException
            // Deal with the exception here, and/or rethrow at your discretion. InternalServiceErrorException
            // An error occurred on the server side. InvalidParameterException
            // You provided an invalid value for a parameter. InvalidRequestException
            // You provided a parameter value that is not valid for the current state of the resource. InvalidRequestException
            // We can't find the resource that you asked for. ResourceNotFoundException
            logger.error(e.getMessage(), e);
            throw e;
        }

        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
            secretValue = secret;
        } else {
            decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
            secretValue = decodedBinarySecret;
        }
        return secretDeserializer(secretValue);
    }

    public static HashMap<String, String> secretDeserializer(String secretsKeyValue) {
        final ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, String> secretMap = null;
        try {
            secretMap = objectMapper.readValue(secretsKeyValue, HashMap.class);
        } catch (IOException e) {
            logger.error("An error occurred parsing Secret String Value from Amazon Secret Manager", e);
        }
        return secretMap;
    }
}
