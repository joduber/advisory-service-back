package com.thehackettgroup.advisory.dao;

import com.thehackettgroup.advisory.dto.Survey;
import com.thehackettgroup.advisory.exception.AdvisoryApplicationException;
import com.thehackettgroup.advisory.rowmapper.SurveyRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class SurveyDao {

    public static final Logger logger = LoggerFactory.getLogger(SurveyDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public UUID addSurvey(Survey survey, UUID masterClientId) {
        UUID surveyId = UUID.randomUUID();
        jdbcTemplate.update("INSERT INTO bpic.survey VALUES (?,?,?,?,?,?,?)", surveyId, masterClientId, survey.getQualtricSurveyId(),
                survey.getSurveyType(), survey.getName().trim(), survey.getDescription().trim(), survey.getLink());
        return surveyId;
    }

    public Optional<Survey> getSurveyById(String surveyId) {
        try {
            UUID uuid = UUID.fromString(surveyId);
            return jdbcTemplate.queryForObject("SELECT survey_id, survey_type_id, qualtrics_survey_id, master_client_id," +
                            " description, \"name\" FROM bpic.survey where survey_id=?",
                    new Object[]{uuid}, new SurveyRowMapper());
        } catch (IllegalArgumentException e) {
            logger.error("Unable to parse surveyId: {}", surveyId);
            throw new AdvisoryApplicationException("SurveyId is not valid: " + surveyId);
        }

    }

}
