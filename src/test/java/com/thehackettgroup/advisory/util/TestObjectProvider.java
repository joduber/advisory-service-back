package com.thehackettgroup.advisory.util;

import com.thehackettgroup.advisory.builder.GenericBuilder;
import com.thehackettgroup.advisory.dto.Survey;

import java.util.Optional;
import java.util.UUID;

public class TestObjectProvider {

    public static Survey getSurvey() {
        return GenericBuilder.of(Survey::new)
                .with(Survey::setName, "Workforce Now Best Practice Exercise")
                .with(Survey::setSurveyId, UUID.fromString("de5c0a7e-64a4-47f3-a331-fa8053f8dd32"))
                .with(Survey::setSurveyType, "BEST_PRACTICE_SCORECARD")
                .with(Survey::setQualtricSurveyId, "SV_4N6fi4q9Lggx3lX")
                .with(Survey::setMasterClientId, UUID.fromString("ac527ad7-b2ee-4715-bc27-f04fd34f3a6e"))
                .with(Survey::setDescription, "Survey Description")
                .build();
    }

    public static Optional<Survey> getOptionalSurvey() {
        return Optional.of(getSurvey());
    }
}
