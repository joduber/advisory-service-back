package com.thehackettgroup.advisory.service;

import com.thehackettgroup.advisory.dao.SurveyDao;
import com.thehackettgroup.advisory.dto.Survey;
import com.thehackettgroup.advisory.exception.RecordNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.util.NestedServletException;

import java.util.Optional;
import java.util.UUID;

import static com.thehackettgroup.advisory.util.TestObjectProvider.getOptionalSurvey;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SurveyServiceTest {

    @Mock
    private SurveyDao surveyDao;

    @InjectMocks
    SurveyService surveyService;

    @Test
    public void shouldReturnSurveyIfExists() {
        Optional<Survey> optionalSurvey = getOptionalSurvey();
        when(surveyDao.getSurveyById(anyString())).thenReturn(optionalSurvey);
        Survey surveyById = surveyService.getSurveyById(UUID.randomUUID().toString());
        Survey expected = optionalSurvey.get();
        assertEquals(expected.getSurveyId(), surveyById.getSurveyId());
    }

    @Test
    public void shouldThrowExceptionIfSurveyDoesNotExists() {
        String surveyId = UUID.randomUUID().toString();
        when(surveyDao.getSurveyById(surveyId)).thenReturn(Optional.empty());
        Assertions.assertThrows(RecordNotFoundException.class, () -> {
            try {
                surveyService.getSurveyById(surveyId);
            } catch (Exception e) {
                assertTrue(e.getMessage().contains(String.format("Survey with ID: %s was not found.", surveyId)));
                throw e;
            }
        });
    }
}
