package com.thehackettgroup.advisory.controller;

import com.thehackettgroup.advisory.builder.GenericBuilder;
import com.thehackettgroup.advisory.dto.Survey;
import com.thehackettgroup.advisory.exception.AdvisoryApplicationException;
import com.thehackettgroup.advisory.service.SurveyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.util.UUID;

import static com.thehackettgroup.advisory.util.TestObjectProvider.getSurvey;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SurveyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SurveyService surveyService;

    @Test
    public void shouldReturnContentIfAvailable() throws Exception {
        when(surveyService.getSurveyById(anyString())).thenReturn(getSurvey());
        this.mockMvc.perform(get("/api/v1/survey/de5c0a7e-64a4-47f3-a331-fa8053f8dd32")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("de5c0a7e-64a4-47f3-a331-fa8053f8dd32")));
    }

    @Test()
    public void shouldNotReturnContentIfAvailable() throws Exception {
        when(surveyService.getSurveyById(anyString())).thenThrow(new AdvisoryApplicationException());
        Assertions.assertThrows(NestedServletException.class, () -> {
            this.mockMvc.perform(get("/api/v1/survey/de5c0a7e-64a4-47f3-a331-fa8053f8dd32")).andDo(print()).andExpect(status().isOk())
                    .andExpect(content().string(containsString("de5c0a7e-64a4-47f3-a331-fa8053f8dd32")));
        });
    }


}
